# 九齐单片机 NY8 例程资源库

本仓库提供了九齐单片机 NY8 全系列的例程资源，涵盖了多个型号，包括 NY8A051D、NY8A053D、NY8A054D、NY8AE51D、NY8B062D、NY8B072A 等。这些例程不仅包含了 C 语言的实现，还提供了汇编语言的代码，方便开发者根据需求选择合适的编程方式。

## 资源内容

- **NY8A051D 例程**：包含 C 语言和汇编语言的实现。
- **NY8A053D 例程**：包含 C 语言和汇编语言的实现。
- **NY8A054D 例程**：包含 C 语言和汇编语言的实现。
- **NY8AE51D 例程**：包含 C 语言和汇编语言的实现。
- **NY8B062D 例程**：包含 C 语言和汇编语言的实现。
- **NY8B072A 例程**：包含 C 语言和汇编语言的实现。

## 使用说明

1. **下载资源**：点击仓库中的相应文件进行下载。
2. **导入工程**：将下载的例程导入到你的开发环境中。
3. **参考学习**：根据例程中的代码进行学习和开发。

## 注意事项

- 请确保你使用的开发环境与例程兼容。
- 在编译和运行例程时，请仔细阅读代码中的注释，以确保正确理解代码逻辑。

希望这些例程能够帮助你更好地理解和使用九齐单片机 NY8 系列，加速你的开发进程。如果有任何问题或建议，欢迎在仓库中提出。